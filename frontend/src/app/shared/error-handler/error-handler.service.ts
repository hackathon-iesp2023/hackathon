import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor() { }

  handlerError(error: any): Observable<any>{
    console.log('Algum error ocorreu {} ', error);
    Swal.fire(
      '',
      `Algum error ocorreu, por favor verifique e tente novamente!`,
      'error'
    )
    return throwError(() => error);
  }
}
