export interface IUserRegistry{
    name: string;
    email: string;
    password: string;
}