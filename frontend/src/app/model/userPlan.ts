export interface IUserPlan{
    email: string;
    question: string;
    answer: string;
}