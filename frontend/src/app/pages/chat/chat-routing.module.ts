import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChatComponent } from './chat.component';
import { HistoricComponent } from './historic/historic.component';

const routes: Routes = [
  {
    path:'',
    component: ChatComponent
  },
  {
    path:'historic',
    component: HistoricComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChatRoutingModule { }
