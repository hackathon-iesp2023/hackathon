import { Component, OnInit } from '@angular/core';
import { IUserPlan } from 'src/app/model/userPlan';
import { ResponseService } from 'src/app/service/chatGpt/response/response.service';

@Component({
  selector: 'app-historic',
  templateUrl: './historic.component.html',
  styleUrls: ['./historic.component.css']
})
export class HistoricComponent implements OnInit{

  response: IUserPlan[];
  public customResponseHeaders: string[] = [
    'Descrição do Plano de Negócio:',
    'Visão do Plano de Negócio:',
    'Missão do plano de negócio:',
    'Metas de Curto Prazo:',
    'Metas de Longo Prazo:',
    'Objetivos:',
    'Estratégia de Mercado:',
    'Projeções Financeiras:',
    'Tendências do Setor:',
    'Público-Alvo:',
    'Fatores Econômicos:',
    'Potencial de Mercado:'
  ];

  constructor(private responseService: ResponseService){
    this.response = [];
  }

  ngOnInit(): void {
    this.getAllResponses();
  }

  getAllResponses(){
    const user = localStorage.getItem('email')!;

    this.responseService.getResponses(user).subscribe((result) => {
      this.response = result;
    });
  }

}
