import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IUserRegistry } from 'src/app/model/userRegistry';
import { LoginService } from 'src/app/service/login/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit{

  formBuilder: FormBuilder;
  userRegistryForm: FormGroup;

  constructor(private router: Router, private injector: Injector, private loginService: LoginService){
    this.formBuilder = this.injector.get(FormBuilder);
    this.userRegistryForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildUserRegistryForm();
  }

  buildUserRegistryForm(){
    this.userRegistryForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.minLength(2)]],
      password: [null, [Validators.required, Validators.minLength(2)]]
    })
  }

  registryUser(){
    const userRegistry: IUserRegistry = this.userRegistryForm.value;

    this.loginService.userRegistry(userRegistry).subscribe((response) => {
      if(response.responseCode === 1){
        this.router.navigate(['/login']);
      }else{
        Swal.fire(
          '',
          'Email já cadastrado, por favor verifique e tente novamente!',
          'error'
        )
      }
      console.log(response);
    })
  }

}
