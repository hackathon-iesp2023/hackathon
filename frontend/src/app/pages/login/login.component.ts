import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { IUser } from 'src/app/model/user';
import { LoginService } from 'src/app/service/login/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  formBuilder: FormBuilder;
  loginForm: FormGroup;

  constructor(private router: Router, private injector: Injector, private loginService: LoginService){
    this.formBuilder = this.injector.get(FormBuilder);
    this.loginForm = this.formBuilder.group({});
  }

  ngOnInit(): void {
    this.buildLoginForm();
  }

  buildLoginForm(){
    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.minLength(2)]],
      password: [null, [Validators.required, Validators.minLength(2)]]
    })
  }

  login(){
    const userLogin: IUser = this.loginForm.value;

    this.loginService.doLogin(userLogin).subscribe((response) => {
      if(response.responseCode === 3){
        console.log(response.responseUserEnum);
        Swal.fire(
          '',
          'Login não autorizado, verifique e tente novamente!',
          'error'
        );
      }else{
        localStorage.setItem('email', userLogin.email);
        this.router.navigate(['/chat']);
      }
    });
  }

}
