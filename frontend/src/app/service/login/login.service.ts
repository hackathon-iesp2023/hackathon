import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError } from 'rxjs';
import { IUser } from 'src/app/model/user';
import { IUserRegistry } from 'src/app/model/userRegistry';
import { ErrorHandlerService } from 'src/app/shared/error-handler/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiUrl: string;
  loginUrl: string;
  signupUrl: string;

  constructor(private httpClient: HttpClient, private handlerError: ErrorHandlerService) {
    this.apiUrl = 'http://localhost:8080/api/v1/users';
    this.loginUrl = '/login';
    this.signupUrl = '/sign-up';
  }

  doLogin(userLogin: IUser): Observable<any>{
    return this.httpClient.post(`${this.apiUrl}${this.loginUrl}`, userLogin)
      .pipe(catchError(this.handlerError.handlerError));
  }

  userRegistry(user: IUserRegistry): Observable<any>{
    return this.httpClient.post(`${this.apiUrl}${this.signupUrl}`, user)
    .pipe(catchError(this.handlerError.handlerError));
  }

}
