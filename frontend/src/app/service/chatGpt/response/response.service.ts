import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError } from 'rxjs';
import { IUserPlan } from 'src/app/model/userPlan';
import { ErrorHandlerService } from 'src/app/shared/error-handler/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  private apiUrl = 'http://localhost:8080/plan';

  constructor(private http: HttpClient, private handlerError:ErrorHandlerService) { }

  saveResponse(response: IUserPlan) {
    return this.http.post(this.apiUrl, response)
      .pipe(catchError(this.handlerError.handlerError));
  }

  getResponses(email: string): Observable<IUserPlan[]>{
    let queryParams = new HttpParams().append("email", email);
    return this.http.get<IUserPlan[]>(`${this.apiUrl}`, {params: queryParams})
      .pipe(catchError(this.handlerError.handlerError));
  }
}
