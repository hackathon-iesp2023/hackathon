package br.com.hackaton.startmeup.request;

import br.com.hackaton.startmeup.entities.Plan;
import br.com.hackaton.startmeup.entities.User;
import lombok.Data;

@Data
public class PlanRequest {

    private String email;
    private String question;
    private String answer;

    public Plan planRequestToPlan(User user) {
        Plan plan = new Plan();

        plan.setQuestion(this.question);
        plan.setAnswer(this.answer);
        plan.setUser(user);

        return plan;
    }

}
