package br.com.hackaton.startmeup.controllers;

import br.com.hackaton.startmeup.entities.Plan;
import br.com.hackaton.startmeup.entities.User;
import br.com.hackaton.startmeup.repositories.PlanRepository;
import br.com.hackaton.startmeup.repositories.UserRepository;
import br.com.hackaton.startmeup.request.PlanRequest;
import br.com.hackaton.startmeup.response.PlanResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/plan")
public class PlanController {

    private PlanRepository planRepository;
    private UserRepository userRepository;

    public PlanController(PlanRepository planRepository, UserRepository userRepository) {
        this.planRepository = planRepository;
        this.userRepository = userRepository;
    }

    @PostMapping
    public ResponseEntity<PlanResponse> save(@RequestBody PlanRequest planRequest){

        User user = userRepository.findByEmail(planRequest.getEmail());

        if(user == null){
            return ResponseEntity.ok(new PlanResponse());
        }

        Plan plan = planRepository.save(planRequest.planRequestToPlan(user));

        return ResponseEntity.ok(new PlanResponse(user.getEmail(), plan.getQuestion(), plan.getAnswer()));

    }

    @GetMapping
    public ResponseEntity<List<PlanResponse>> listUserPlan(@RequestParam String email){
        User user = userRepository.findByEmail(email);

        List<Plan> listPlan = planRepository.findByUser(user);
        List<PlanResponse> listPlanResponse = new ArrayList();

        for(Plan plan:listPlan) {
            listPlanResponse.add(new PlanResponse(plan.getUser().getEmail(), plan.getQuestion(), plan.getAnswer()));
        }

        return ResponseEntity.ok(listPlanResponse);

    }

}
