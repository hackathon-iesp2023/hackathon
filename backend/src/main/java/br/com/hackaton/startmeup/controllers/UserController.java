package br.com.hackaton.startmeup.controllers;

import br.com.hackaton.startmeup.repositories.UserRepository;
import br.com.hackaton.startmeup.request.UserRequest;
import br.com.hackaton.startmeup.request.UserRequestLogin;
import br.com.hackaton.startmeup.response.UserResponse;
import br.com.hackaton.startmeup.utils.ResponseUserEnum;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("api/v1/users")
public class UserController {

    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<UserResponse> signIn(@RequestBody UserRequest userRequest) throws NoSuchAlgorithmException {
        userRequest.setPassword(sha256(userRequest.getPassword()));
        var user = userRepository.save(userRequest.userRequestToUserEntity());

        if(user == null) return null;

        return ResponseEntity.ok(new UserResponse(ResponseUserEnum.CREATED.getId(),
                ResponseUserEnum.CREATED.getDescription()));
    }

    @PostMapping("/login")
    public ResponseEntity<UserResponse> login(@RequestBody UserRequestLogin userRequestLogin) throws NoSuchAlgorithmException {
        var user = userRepository.findByEmail(userRequestLogin.getEmail());

        if(user != null && user.getPassword().equals(sha256(userRequestLogin.getPassword()))){
            return ResponseEntity.ok(new UserResponse(ResponseUserEnum.ACCESS_GRANTED.getId(),
                    ResponseUserEnum.ACCESS_GRANTED.getDescription()));
        }

        return ResponseEntity.ok(new UserResponse(ResponseUserEnum.PERMISSION_DENIED.getId(),
                ResponseUserEnum.PERMISSION_DENIED.getDescription()));
    }

    private String sha256(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(input.getBytes());
        byte[] digest = md.digest();
        StringBuilder sb = new StringBuilder();
        for (byte b : digest) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

}
