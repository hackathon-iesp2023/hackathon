package br.com.hackaton.startmeup.repositories;

import br.com.hackaton.startmeup.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
}
