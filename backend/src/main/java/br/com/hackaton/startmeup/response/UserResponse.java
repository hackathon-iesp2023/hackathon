package br.com.hackaton.startmeup.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserResponse {

    public Long responseCode;
    public String responseUserEnum;

}
