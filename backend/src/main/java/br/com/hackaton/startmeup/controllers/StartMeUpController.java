package br.com.hackaton.startmeup.controllers;

import br.com.hackaton.startmeup.service.StartMeUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/startMeUp")
public class StartMeUpController {

    @Autowired
    private StartMeUpService startmeupService;

    @GetMapping
    public String getMessage(@RequestParam String message) {

        return startmeupService.chatGpt(message);
    }


}
