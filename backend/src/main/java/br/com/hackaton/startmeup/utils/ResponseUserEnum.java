package br.com.hackaton.startmeup.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResponseUserEnum {

    CREATED(1L, "CREATED"),
    UPDATED(2L, "UPDATED"),
    PERMISSION_DENIED(3L, "PERMISSION_DENIED"),
    ACCESS_GRANTED(4L, "ACCESS_GRANTED")
    ;

    private Long id;
    private String description;
}
