package br.com.hackaton.startmeup.service;

import br.com.hackaton.startmeup.utils.ApiConstants;
import com.theokanning.openai.OpenAiService;
import com.theokanning.openai.completion.CompletionRequest;
import org.springframework.stereotype.Service;

@Service
public class StartMeUpService {

    OpenAiService api = new OpenAiService(ApiConstants.API_KEY);

    private static final String TEXT = "text-davinci-003";

    public String chatGpt(String message) {

        CompletionRequest completionRequest = CompletionRequest.builder()
                .model(TEXT)
                .prompt(message)
                .maxTokens(500)
                .build();

        return api.createCompletion(completionRequest).getChoices().get(0).getText();


    }

}

