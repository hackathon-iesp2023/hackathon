package br.com.hackaton.startmeup.request;

import br.com.hackaton.startmeup.entities.User;
import lombok.Data;

@Data
public class UserRequestLogin {

    private String email;
    private String password;

    public User requestLoginToUserEntity() {
        User user = new User();

        user.setEmail(this.getEmail());
        user.setPassword(this.getPassword());

        return user;
    }
}
