package br.com.hackaton.startmeup.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlanResponse {

    private String email;
    private String question;
    private String answer;
}
