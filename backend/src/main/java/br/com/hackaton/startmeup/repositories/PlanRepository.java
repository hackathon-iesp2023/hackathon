package br.com.hackaton.startmeup.repositories;

import br.com.hackaton.startmeup.entities.Plan;
import br.com.hackaton.startmeup.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlanRepository extends JpaRepository<Plan, Long> {

    List<Plan> findByUser(User user);
}
