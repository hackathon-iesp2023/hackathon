package br.com.hackaton.startmeup.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "TB_PLAN")
public class Plan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ID_USER")
    private User user;
    @Column(name = "NAM_QUESTION")
    private String question;

    @Column(name = "NAM_ANSWER")
    private String answer;

}
