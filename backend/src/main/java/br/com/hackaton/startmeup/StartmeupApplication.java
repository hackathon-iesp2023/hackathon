package br.com.hackaton.startmeup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StartmeupApplication {

	public static void main(String[] args) {
		SpringApplication.run(StartmeupApplication.class, args);
	}

}
