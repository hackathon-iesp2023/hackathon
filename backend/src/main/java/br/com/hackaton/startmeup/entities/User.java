package br.com.hackaton.startmeup.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "TB_USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAM_NAME")
    private String name;
    @Column(name = "NAM_EMAIL")
    private String email;
    @Column(name = "NAM_PASSWORD")
    private String password;

}
