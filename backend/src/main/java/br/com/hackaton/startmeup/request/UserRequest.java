package br.com.hackaton.startmeup.request;

import br.com.hackaton.startmeup.entities.User;
import lombok.Data;

@Data
public class UserRequest {

    private String name;
    private String email;
    private String password;

    public User userRequestToUserEntity() {
        User user = new User();

        user.setEmail(this.getEmail());
        user.setName(this.getName());
        user.setPassword(this.getPassword());

        return user;
    }
}
